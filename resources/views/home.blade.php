<!-- resources/views/auth/home.blade.php -->
{{-- Web site Title --}}

<?php
$fullname = Auth::user()->name;
$stats = DB::table('api_stats')->where('id_user', Auth::user()->id)->first();
$user_sub = DB::table('user_sub')->where('id_user', Auth::user()->id)->first();
$sub = DB::table('subscription')->where('id', $user_sub->id_sub)->first();
$months = $sub->duration;
$daysleft = 0;
if ($months == 0)
{
  $months = 'Unlimited';
  $daysleft = 'Unlimited';
}
else
{
  //die('+' . $months . ' months');
  $expiredate = strtotime("+" . $months . " months", strtotime($user_sub->updated_at));
  $daysleft = floor(($expiredate - time())/(60*60*24));
}

?>

@extends('layouts.default')
{{-- Content --}}
@section('content')


  <body>
  <!-- container section start -->
  <section id="container" class="">
     
      
      <header class="header dark-bg">
            <div class="toggle-nav">
                <div class="icon-reorder tooltips" data-original-title="Toggle Navigation" data-placement="bottom"></div>
            </div>

            <!--logo start-->
            <a href="{{ URL::asset('index.html') }}" class="logo">Show<span class="lite">Lytics</span></a>
            <!--logo end-->


            <div class="top-nav notification-row">
                <!-- notificatoin dropdown start-->
                <ul class="nav pull-right top-menu">

                    <!-- user login dropdown start-->
                    <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="profile-ava">
                                <img alt="" src="{{ URL::asset('img/avatar1_small.jpg') }}">
                            </span>
                            <span class="username">{{ $fullname }}</span>
                            <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu extended logout">
                            <div class="log-arrow-up"></div>
                            <li>
                                <a href="{!! URL::to('/auth/logout') !!}"><i class="icon_key_alt"></i> Log Out</a>
                            </li>
                        </ul>
                    </li>
                    <!-- user login dropdown end -->
                </ul>
                <!-- notificatoin dropdown end-->
            </div>
      </header>      
      <!--header end-->

      <!--sidebar start-->
      <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu">                
                  <li class="active">
                      <a class="" href="{{ URL::asset('home') }}">
                          <i class="icon_house_alt"></i>
                          <span>Dashboard</span>
                      </a>
                  </li>

                  <li class="active">
                      <a class="" href="{{ URL::asset('profile') }}">
                          <i class="icon_profile"></i>
                          <span>My profile</span>
                      </a>
                  </li>

                  <li>
                      <a class="" href="{{ URL::asset('subscription') }}">
                          <i class="icon_genius"></i>
                          <span>My subscription</span>
                      </a>
                  </li>
                  
              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->
      
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">            
              <!--overview start-->
			  <div class="row">
				<div class="col-lg-12">
					<h3 class="page-header"><i class="fa fa-laptop"></i> Dashboard</h3>
					<ol class="breadcrumb">
						<li><i class="fa fa-home"></i><a href="{{ URL::asset('index.html') }}">Home</a></li>
						<li><i class="fa fa-laptop"></i>Dashboard</li>						  	
					</ol>
				</div>
			</div>
              
            <div class="row">
				<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
					<div class="info-box blue-bg">
						<i class="fa fa-cloud-download"></i>
						<div class="count">{{ $stats->api_calls }}</div>
						<div class="title">Times you used Showlytics API</div>
					</div><!--/.info-box-->			
				</div><!--/.col-->
				
				<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
					<div class="info-box brown-bg">
						<i class="fa fa-shopping-cart"></i>
						<div class="count">{{ $months }} Months</div>
						<div class="title">Subscription</div>
					</div><!--/.info-box-->			
				</div><!--/.col-->	
				
				<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
					<div class="info-box dark-bg">
						<i class="fa fa-thumbs-o-up"></i>
						<div class="count">{{ $daysleft }}</div>
						<div class="title">Days left</div>
					</div><!--/.info-box-->			
				</div><!--/.col-->
				
				<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
					<div class="info-box green-bg">
						<i class="fa fa-cubes"></i>
						<div class="count">{{ $stats->total_fetched }}</div>
						<div class="title">Data results</div>
					</div><!--/.info-box-->			
				</div><!--/.col-->
				
			</div><!--/.row-->

			<div class="row">
                    <section class="panel">
                        <header class="panel-heading">
                            Your usage of Showlytics during the past month
                        </header>
                        <div class="panel-body text-center">
                            <canvas id="line" height="300" width="450"></canvas>
                        </div>
                    </section>

            </div><!--/col-->

          </section>
      </section>
      <!--main content end-->
  </section>
  <!-- container section start -->

  <!-- javascripts -->
  <script src="{{ URL::asset('js/jquery.js') }}"></script>
  <script src="{{ URL::asset('js/jquery-ui-1.10.4.min.js') }}"></script>
  <script src="{{ URL::asset('js/jquery-1.8.3.min.js') }}"></script>
  <script type="text/javascript" src="{{ URL::asset('js/jquery-ui-1.9.2.custom.min.js') }}"></script>
  <!-- bootstrap -->
  <script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
  <!-- nice scroll -->
  <script src="{{ URL::asset('js/jquery.scrollTo.min.js') }}"></script>
  <script src="{{ URL::asset('js/jquery.nicescroll.js') }}" type="text/javascript"></script>
  <!-- charts scripts -->
  <script src="{{ URL::asset('assets/jquery-knob/js/jquery.knob.js') }}"></script>
  <script src="{{ URL::asset('js/jquery.sparkline.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js') }}"></script>
  <script src="{{ URL::asset('js/owl.carousel.js') }}" ></script>
  <!-- jQuery full calendar -->
  <script src="{{ URL::asset('js/fullcalendar.min.js') }}"></script>
  <!-- Full Google Calendar - Calendar -->
  <script src="{{ URL::asset('assets/fullcalendar/fullcalendar/fullcalendar.js') }}"></script>
  <!--script for this page only-->
  <script src="{{ URL::asset('js/calendar-custom.js') }}"></script>
  <script src="{{ URL::asset('js/jquery.rateit.min.js') }}"></script>
  <!-- custom select -->
  <script src="{{ URL::asset('js/jquery.customSelect.min.js') }}" ></script>
  <script src="{{ URL::asset('assets/chart-master/Chart.js') }}"></script>

  <!--custome script for all page-->
  <script src="{{ URL::asset('js/scripts.js') }}"></script>
  <!-- custom script for this page-->
  <script src="{{ URL::asset('js/sparkline-chart.js') }}"></script>
  <script src="{{ URL::asset('js/easy-pie-chart.js') }}"></script>
  <script src="{{ URL::asset('js/jquery-jvectormap-1.2.2.min.js') }}"></script>
  <script src="{{ URL::asset('js/jquery-jvectormap-world-mill-en.js') }}"></script>
  <script src="{{ URL::asset('js/xcharts.min.js') }}"></script>
  <script src="{{ URL::asset('js/jquery.autosize.min.js') }}"></script>
  <script src="{{ URL::asset('js/jquery.placeholder.min.js') }}"></script>
  <script src="{{ URL::asset('js/gdp-data.js') }}"></script>
  <script src="{{ URL::asset('js/morris.min.js') }}"></script>
  <script src="{{ URL::asset('js/sparklines.js') }}"></script>
  <script src="{{ URL::asset('js/charts.js') }}"></script>
  <script src="{{ URL::asset('js/jquery.slimscroll.min.js') }}"></script>
  <script>

      //knob
      $(function() {
        $(".knob").knob({
          'draw' : function () { 
            $(this.i).val(this.cv + '%')
          }
        })
      });

      //carousel
      $(document).ready(function() {
          $("#owl-slider").owlCarousel({
              navigation : true,
              slideSpeed : 300,
              paginationSpeed : 400,
              singleItem : true

          });
      });

      //custom select box

      $(function(){
          $('select.styled').customSelect();
      });

	  /* ---------- Map ---------- */
	$(function(){
	  $('#map').vectorMap({
	    map: 'world_mill_en',
	    series: {
	      regions: [{
	        values: gdpData,
	        scale: ['#000', '#000'],
	        normalizeFunction: 'polynomial'
	      }]
	    },
		backgroundColor: '#eef3f7',
	    onLabelShow: function(e, el, code){
	      el.html(el.html()+' (GDP - '+gdpData[code]+')');
	    }
	  });
	});

  </script>
