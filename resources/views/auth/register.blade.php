<!-- resources/views/auth/register.blade.php -->
{{-- Web site Title --}}
@extends('layouts.default')
{{-- Content --}}
@section('content')

    <body class="login-img3-body">

    <div class="container">

        <form class="login-form" role="form" method="post" action="{!! URL::to('/auth/register')
!!}">
            {!! csrf_field() !!}
            <div class="login-wrap">
                <p class="login-img"><i class="icon_lock_alt"></i></p>
                <div class="input-group">
                    <span class="input-group-addon"><i class="icon_profile"></i></span>
                    <input type="text" class="form-control" placeholder="Full name" name=
                    "name" value="{{ old('name') }}" autofocus>
                </div>
                <div class="input-group">
                    <span class="input-group-addon"><i class="icon_profile"></i></span>
                    <input type="email" class="form-control" placeholder="Email address" name=
                    "email" value="{{ old('email') }}">
                </div>

                <div class="input-group">
                    <span class="input-group-addon"><i class="icon_profile"></i></span>
                    <input type="text" class="form-control" placeholder="Trakt username" name=
                    "trakt_username" value="{{ old('trakt_username') }}">
                </div>

                <div class="input-group">
                    <span class="input-group-addon"><i class="icon_key_alt"></i></span>
                    <input type="password" class="form-control" placeholder="Password" name=
                    "password">
                </div>
                <div class="input-group">
                    <span class="input-group-addon"><i class="icon_key_alt"></i></span>
                    <input type="password" class="form-control" placeholder="Confirm password" name=
                    "password_confirmation">
                </div>

                <button class="btn btn-primary btn-lg btn-block" type="submit" name="login" value=
                "login">Register</button>
            </div>
        </form>

    </div>

    </body>