<!-- resources/views/auth/login.blade.php -->
{{-- Web site Title --}}
@extends('layouts.default')
{{-- Content --}}
@section('content')

<body class="login-img3-body">

<div class="container">
    <form class="login-form" role="form" method="post" action="{!! URL::to('/auth/login') !!}">
        {!! csrf_field() !!}
        <div class="login-wrap">
            <p class="login-img"><i class="icon_lock_alt"></i></p>
            <div class="input-group">
                <span class="input-group-addon"><i class="icon_profile"></i></span>
                <input type="email" class="form-control" placeholder="Email address" name="email"
                       autofocus>
            </div>
            <div class="input-group">
                <span class="input-group-addon"><i class="icon_key_alt"></i></span>
                <input type="password" class="form-control" placeholder="Password" name="password">
            </div>
            <label class="checkbox">
                <input type="checkbox" name="remember"> Remember me
                <span class="pull-right"> <a href="#"> Forgot Password?</a></span>
            </label>
            <button class="btn btn-primary btn-lg btn-block" type="submit" name="login" value="login">Login</button>
            <button class="btn btn-info btn-lg btn-block" type="submit" name="register" value="register">Not registered yet ? Signup
            </button>
        </div>
    </form>
</div>
</body>
