<!doctype html>
<html>
    <head>
        @include('includes.head')
    </head>
        <div class="container">
                @yield('content')
        </div>
</html>