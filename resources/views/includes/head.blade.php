<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Analytics for TV shows">
<link rel="icon" type="image/png" href="{{ URL::asset('img/favicon_tv.png') }}">

<title>Showlytics</title>

<!-- Bootstrap CSS -->
<link href="{{ URL::asset('css/bootstrap.min.css') }}" rel="stylesheet">
<!-- bootstrap theme -->
<link href="{{ URL::asset('css/bootstrap-theme.css') }}" rel="stylesheet">
<!--external css-->
<!-- font icon -->
<link href="{{ URL::asset('css/elegant-icons-style.css') }}" rel="stylesheet" />
<link href="{{ URL::asset('css/font-awesome.css') }}" rel="stylesheet" />
<!-- full calendar css-->
<link href="{{ URL::asset('assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css') }}" rel=
"stylesheet" />
<link href="{{ URL::asset('assets/fullcalendar/fullcalendar/fullcalendar.css') }}" rel="stylesheet" />
<!-- easy pie chart-->
<link href="{{ URL::asset('assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css') }}" rel="stylesheet" type="text/css" media="screen"/>
<!-- owl carousel -->
<link rel="{{ URL::asset('stylesheet" href="css/owl.carousel.css') }}" type="text/css">
<link href="{{ URL::asset('css/jquery-jvectormap-1.2.2.css') }}" rel="stylesheet">
<!-- Custom styles -->
<link href="{{ URL::asset('css/style.css') }}" rel="stylesheet">
<link href="{{ URL::asset('css/style-responsive.css') }}" rel="stylesheet" />
<link rel="stylesheet" href="{{ URL::asset('css/fullcalendar.css') }}">
<link href="{{ URL::asset('css/widgets.css') }}" rel="stylesheet">
<link href="{{ URL::asset('css/xcharts.min.css') }}" rel=" stylesheet">
<link href="{{ URL::asset('css/jquery-ui-1.10.4.min.css') }}" rel="stylesheet">

<!-- HTML5 shim and Respond.js IE8 support of HTML5 -->
<!--[if lt IE 9]>
<script src="{{ URL::asset('js/html5shiv.js') }}"></script>
<script src="{{ URL::asset('js/respond.min.js') }}"></script>
<script src="{{ URL::asset('js/lte-ie7.js') }}"></script>
<![endif]-->