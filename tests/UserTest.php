<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testConnexionFail1()
    {
        $this->visit('/')
             ->press('login')
             ->seePageIs('/auth/login');
    }

    public function testRegister()
    {
        $credentials = array(
                'name' => 'test',
                'trakt_username' => 'ekinoxx',
                'email'=>'test@test.fr',
                'password'=>'azerty',
                'password_confirmation'=>'azerty',
                'csrf_token' => csrf_token()
        );

        $response = $this->action('POST', 'Auth\AuthController@postRegister', null, $credentials);
        // if success user should be redirected to homepage
        $this->seeInDatabase('users', ['email' => 'test@test.fr']);
    }

    public function testConnexionFail2()
    {
        $this->visit('/')
             ->type('test@test.fr', 'email')
             ->type('test', 'password')
             ->press('login')
             ->seePageIs('/auth/login');
    }

    public function testConnexionSuccess()
    {
        $this->visit('/')
             ->type('test@test.fr', 'email')
             ->type('azerty', 'password')
             ->press('login')
             ->seePageIs('/home');
    }
}
