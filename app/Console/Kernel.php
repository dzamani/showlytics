<?php

namespace App\Console;

use DB;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\Inspire::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {
            $users = DB::table('users')->get();
            if (is_array($users) || is_object($users))
            {
                foreach ($users as $user) {
                    $user_sub = DB::table('user_sub')->where('id_user', $user->id)->first();
                    DB::table('api_stats')->where('id_user', $user->id)->update(['api_calls' => 0, 'total_fetched' => 0]);
                }
            }
        })->monthly();
    }
}
