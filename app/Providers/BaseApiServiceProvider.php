<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class BaseApiServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        // Register 'api' instance container to our Request object
        $this->app['baseapi'] = $this->app->share(function($app)
        {
            return new \App\BaseApi\BaseApiRequest;
        });
    }
}
