<?php

namespace App\BaseApi;

class BaseApiRequest
{
	private static $domain = 'https://api-v2launch.trakt.tv';

	public static $requestHeaders = array(
		'Content-Type:application/json',
		'trakt-api-key:88946908191fb9cb8ca7f7e8f7b603d809510eb836101ea5f7b026f29e9546ba',
		'trakt-api-version:2',
		'X-Pagination-Page:1',
		'X-Pagination-Limit:5'
	);

	public function __construct()
	{
		$this->curl = curl_init();
		curl_setopt_array($this->curl, [
				CURLOPT_SSL_VERIFYPEER => true,
				CURLOPT_RETURNTRANSFER => 1,
				CURLOPT_HTTPHEADER => self::$requestHeaders,
				CURLOPT_FOLLOWLOCATION => 1,
				CURLOPT_VERBOSE => 1,
			]
		);
	}

	public function __destruct()
	{
		curl_close($this->curl);
	}

	public function get($path, $params = [])
	{
		curl_setopt_array(
			$this->curl, [
				CURLOPT_URL => self::$domain . '/' . $path . '/?' . http_build_query($params),
			]
		);

		return json_decode(curl_exec($this->curl));
	}

	public function post($path, $params = [])
	{
		curl_setopt_array(
			$this->curl, [
				CURLOPT_URL => self::$domain . '/' . $path,
				CURLOPT_POST => 1,
				CURLOPT_POSTFIELDS => $params,
			]
		);

		return curl_exec($this->curl);
	}
}
