<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Facades\BaseApi;
use DB;
use Cache;

class FriendsController extends Controller
{
    public function lastWatched($api_key)
    {
        $usertrakt = DB::table('int_user_trakt')->where('api_key', $api_key)->first();
        if ($usertrakt)
        {
            $username = $usertrakt->trakt_username;
            $user_sub = DB::table('user_sub')->where('id_user', $usertrakt->id_user)->first();
            if ($user_sub->id_sub == 1)
            {
                $stats = DB::table('api_stats')->where('id_user', $usertrakt->id_user)->first();
                if ($stats->api_calls > 100)
                {
                    return response()->json(array('error' => array('code' => '2', 'message' => 'Call limit exceed, please subscribe to fully use the API.')));
                }
            }

            $friendsArray = BaseApi::get('users/' . $username . '/friends');
            $lastshows = [];

            if (is_array($friendsArray) || is_object($friendsArray))
            {
                foreach ($friendsArray as $value) {
                    $usernameFriend = $value->user->username;

                    $cache_user = Cache::remember($usernameFriend . '_id', 5, function() use ($usernameFriend) {
                        return DB::table('cache_user')->where('trakt_username', $usernameFriend)->first();
                    });
                    $cache_user_id = 0;
                    if (!$cache_user)
                    {
                        $cache_user_id = DB::table('cache_user')->insertGetId(
                            array(
                                'trakt_username' => $usernameFriend
                            )
                        );

                        Cache::add($usernameFriend . '_id', 
                            DB::table('cache_user')->where('id', $cache_user_id)->first(), 5);
                    }
                    else
                    {
                        $cache_user_id = $cache_user->id;
                    }

                    $cache_user_watched = Cache::remember($usernameFriend . '_watched', 5, function() use ($cache_user_id) {
                        return DB::table('cache_user_watched')->where('id_cache_user', $cache_user_id)->first();
                    });

                    if (!$cache_user_watched)
                    {
                        $shows = BaseApi::get('users/' . $usernameFriend . '/history/shows');
                        $episodes = BaseApi::get('users/' . $usernameFriend . '/history/episodes');
                        if ($shows == null)
                            $shows = array();
                        if ($episodes == null)
                            $episodes = array();
                        $merge = array_merge($shows, $episodes);

                        $cache_user_watched_id = DB::table('cache_user_watched')->insertGetId(
                            array(
                                'id_cache_user' => $cache_user_id,
                                'data' => json_encode($merge),
                                'updated_at' => DB::raw('NOW()'),
                            )
                        );

                        Cache::add($usernameFriend . '_watched', 
                            DB::table('cache_user_watched')->where('id', $cache_user_watched_id)->first(), 5);

                        $lastshows[$usernameFriend] = $merge;
                    }
                    else
                    {
                        if ((time() - strtotime($cache_user_watched->updated_at)) > (60 * 15))
                        {
                            $shows = BaseApi::get('users/' . $usernameFriend . '/history/shows');
                            $episodes = BaseApi::get('users/' . $usernameFriend . '/history/episodes');
                            if ($shows == null)
                                $shows = array();
                            if ($episodes == null)
                                $episodes = array();
                            $merge = array_merge($shows, $episodes);
                            DB::table('cache_user_watched')->where('id', $cache_user_watched->id)
                            ->update(['data' => json_encode($merge), 'updated_at' => DB::raw('NOW()')]);
                        }

                        $lastshows[$usernameFriend] = json_decode($cache_user_watched->data);
                    }
                }
            }

            DB::table('api_stats')->where('id', $usertrakt->id_user)->increment('api_calls');
            DB::table('api_stats')->where('id', $usertrakt->id_user)->increment('total_fetched', count($friendsArray));

            return $lastshows;
        }
        else
        {
            return response()->json(array('error' => array('code' => '1', 'message' => 'ApiKey not found.')));
        }
    }

    public function currentlyWatching($api_key)
    {
        $usertrakt = DB::table('int_user_trakt')->where('api_key', $api_key)->first();
        if ($usertrakt)
        {
            $username = $usertrakt->trakt_username;
            $user_sub = DB::table('user_sub')->where('id_user', $usertrakt->id_user)->first();
            if ($user_sub->id_sub == 1)
            {
                $stats = DB::table('api_stats')->where('id_user', $usertrakt->id_user)->first();
                if ($stats->api_calls > 100)
                {
                    return response()->json(array('error' => array('code' => '2', 'message' => 'Call limit exceed, please subscribe to fully use the API.')));
                }
            }

            $friendsArray = BaseApi::get('users/' . $username . '/friends');
            $lastshows = [];

            if (is_array($friendsArray) || is_object($friendsArray))
            {
                foreach ($friendsArray as $value) {
                    $usernameFriend = $value->user->username;

                    $cache_user = Cache::remember($usernameFriend . '_id', 5, function() use ($usernameFriend) {
                        return DB::table('cache_user')->where('trakt_username', $usernameFriend)->first();
                    });
                    $cache_user_id = 0;
                    if (!$cache_user)
                    {
                        $cache_user_id = DB::table('cache_user')->insertGetId(
                            array(
                                'trakt_username' => $usernameFriend
                            )
                        );

                        Cache::add($usernameFriend . '_id', 
                            DB::table('cache_user')->where('id', $cache_user_id)->first(), 5);
                    }
                    else
                    {
                        $cache_user_id = $cache_user->id;
                    }

                    $cache_user_watching = Cache::remember($usernameFriend . '_watching', 5, function() use ($cache_user_id) {
                        return DB::table('cache_user_watching')->where('id_cache_user', $cache_user_id)->first();
                    });

                    if (!$cache_user_watching)
                    {
                        $merge = BaseApi::get('users/' . $usernameFriend . '/watching');
                        if ($merge == null)
                            $merge = array();

                        $cache_user_watching_id = DB::table('cache_user_watching')->insertGetId(
                            array(
                                'id_cache_user' => $cache_user_id,
                                'data' => json_encode($merge),
                                'updated_at' => DB::raw('NOW()'),
                            )
                        );

                        Cache::add($usernameFriend . '_watching', 
                            DB::table('cache_user_watching')->where('id', $cache_user_watching_id)->first(), 5);

                        $lastshows[$usernameFriend] = $merge;
                    }
                    else
                    {
                        if ((time() - strtotime($cache_user_watching->updated_at)) > (60 * 15))
                        {
                            $merge = BaseApi::get('users/' . $usernameFriend . '/watching');
                            if ($merge == null)
                                $merge = array();
                            DB::table('cache_user_watching')->where('id', $cache_user_watching->id)
                            ->update(['data' => json_encode($merge), 'updated_at' => DB::raw('NOW()')]);
                        }

                        $lastshows[$usernameFriend] = json_decode($cache_user_watching->data);
                    }
                }
            }

            DB::table('api_stats')->where('id', $usertrakt->id_user)->increment('api_calls');
            DB::table('api_stats')->where('id', $usertrakt->id_user)->increment('total_fetched', count($friendsArray));

            return $lastshows;
        }
        else
        {
            return response()->json(array('error' => array('code' => '1', 'message' => 'ApiKey not found.')));
        }
    }
}