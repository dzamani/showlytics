<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use DB;
use Auth;
use Illuminate\Support\Facades\Redirect;


class UserController extends Controller
{
    public function updateSubscription($id)
    {
        $user_sub = DB::table('user_sub')->where('id_user', Auth::user()->id)->first();
        $choice = $id;
        if ($choice != $user_sub->id_sub)
        {
            DB::table('user_sub')->where('id_user', Auth::user()->id)->update(['id_sub' => $choice]);
        }

        return Redirect::to('home');
    }
}