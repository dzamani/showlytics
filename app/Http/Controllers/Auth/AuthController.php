<?php

namespace App\Http\Controllers\Auth;

use DB;
use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers;

    protected $redirectTo = '/';
    protected $redirectPath = 'home';
    protected $redirectAfterLogout = 'auth/login';

    public function postAuth(Request $request) {
        $action = "";

        //check which submit was clicked on
        if( $request->input('login')) {
            $action = 'postLogin';
            //$this->postLogin($request); //if login then use this method
        } elseif ($request->input('register')) {
            $action = 'getRegister';
            //$this->postRegister($request); //if register then use this method
        }
        return $this->$action($request);
    }

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'getLogout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
            'trakt_username' => 'required'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $usercreated =  User::create([
                            'name' => $data['name'],
                            'email' => $data['email'],
                            'password' => bcrypt($data['password']),
                        ]);

        DB::table('int_user_trakt')->insert(
            array(
                array(
                    'id_user' => $usercreated->id,
                    'trakt_username' => $data['trakt_username'],
                    'api_key' => md5(uniqid($usercreated->email, true))
                ),
            )
        );

        DB::table('user_sub')->insert(
            array(
                array(
                    'id_user' => $usercreated->id,
                    'id_sub' => 1
                ),
            )
        );

        DB::table('api_stats')->insert(
            array(
                array(
                    'id_user' => $usercreated->id,
                    'api_calls' => 0,
                    'total_fetched' => 0
                ),
            )
        );

        return $usercreated;
    }
}
