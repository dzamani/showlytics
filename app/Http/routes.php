<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postAuth');
Route::get('auth/logout', 'Auth\AuthController@getLogout');


// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');

Route::get('/', function () {
    return Redirect::to('home');
});

Route::get('home', ['middleware' => 'auth', function() {
    return view('home');
}]);

Route::get('profile', ['middleware' => 'auth', function() {
    return view('profile');
}]);

Route::get('subscription', ['middleware' => 'auth', function() {
    return view('subscription');
}]);

Route::get('changeSub/{id}', ['middleware' => 'auth', 'uses' => 'UserController@updateSubscription'] );

Route::group(array('prefix' => 'api/v1'), function()
{
    Route::get('followers/watched/{api_key}', 'Api\FollowersController@lastWatched'); 
    Route::get('followers/watching/{api_key}', 'Api\FollowersController@currentlyWatching'); 

    Route::get('friends/watched/{api_key}', 'Api\FriendsController@lastWatched'); 
    Route::get('friends/watching/{api_key}', 'Api\FriendsController@currentlyWatching'); 

    Route::get('following/watched/{api_key}', 'Api\FollowingController@lastWatched'); 
    Route::get('following/watching/{api_key}', 'Api\FollowingController@currentlyWatching'); 
});


