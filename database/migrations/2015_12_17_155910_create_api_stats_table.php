<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApiStatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('api_stats', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_user')->unsigned();
            $table->integer('api_calls')->unsigned();
            $table->integer('total_fetched')->unsigned();
        });

        Schema::table('api_stats', function($table) {
            $table->foreign('id_user')->references('id')->on('users')->onDelete('cascade');
        });

        DB::table('api_stats')->insert(
            array(
                array(
                    'id_user' => 1,
                    'api_calls' => 0,
                    'total_fetched' => 0
                ),
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('api_stats');
    }
}
