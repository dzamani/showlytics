<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscriptionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscription', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('duration');
        });

        DB::table('subscription')->insert(
            array(
                array(
                    'name' => 'free',
                    'duration' => 0
                ),
                array(
                    'name' => 'monthly',
                    'duration' => 1
                ),
                array(
                    'name' => 'twice yearly',
                    'duration' => 6
                ),
                array(
                    'name' => 'yearly',
                    'duration' => 12
                ),
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('subscription');
    }
}
