<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIntUserTraktTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('int_user_trakt', function (Blueprint $table) {
            $table->increments('id');
            $table->string('trakt_username');
            $table->string('api_key');
            $table->integer('id_user')->unsigned();
        });

        Schema::table('int_user_trakt', function($table) {
            $table->foreign('id_user')->references('id')->on('users')->onDelete('cascade');
        });

        DB::table('int_user_trakt')->insert(
            array(
                array(
                    'trakt_username' => 'ekinoxx',
                    'api_key' => 42,
                    'id_user' => 1
                ),
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('int_user_trakt');
    }
}
