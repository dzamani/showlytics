<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password', 60);
            $table->rememberToken();
            $table->date('created_at'); 
            $table->date('updated_at'); 
        });

        DB::table('users')->insert(
            array(
                array(
                    'name' => 'test',
                    'email' => 'test@test.fr',
                    'password' => '$2y$10$9VNamYHxvvkkZw.AU7ypqeb9lRzSBzjVSzBd9MTgj4haQfTJ5fkg.',
                    'created_at' => '2015-06-17 12:09:42',
                    'updated_at' => '2015-06-17 12:09:42'
                ),
                array(
                    'name' => 'toto',
                    'email' => 'toto@test.fr',
                    'password' => '$2y$10$9VNamYHxvvkkZw.AU7ypqeb9lRzSBzjVSzBd9MTgj4haQfTJ5fkg.',
                    'created_at' => '2015-06-17 12:09:42',
                    'updated_at' => '2015-06-17 12:09:42'
                ),
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
