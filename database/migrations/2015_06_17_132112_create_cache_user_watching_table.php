<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCacheUserWatchingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cache_user_watching', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_cache_user')->unsigned();
            $table->json('data');
            $table->date('updated_at'); 
        });

        Schema::table('cache_user_watching', function($table) {
            $table->foreign('id_cache_user')->references('id')->on('cache_user')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cache_user_watching');
    }
}
