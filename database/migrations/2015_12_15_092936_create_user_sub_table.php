<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserSubTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_sub', function (Blueprint $table) {
            $table->integer('id_user')->unsigned();
            $table->integer('id_sub')->unsigned();
            $table->date('created_at'); 
            $table->date('updated_at'); 
        });

        Schema::table('user_sub', function($table) {
            $table->foreign('id_user')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('id_sub')->references('id')->on('subscription')->onDelete('cascade');
        });

        DB::table('user_sub')->insert(
            array(
                array(
                    'id_user' => 1,
                    'id_sub' => 1,
                    'created_at' => DB::raw('NOW()'),
                    'updated_at' => DB::raw('NOW()')
                ),
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_sub');
    }
}
